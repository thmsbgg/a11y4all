//--------------------------------------------------------------------------
// Grunt tasks
//--------------------------------------------------------------------------
//
// You can use -verbose param to see if everything run well
//
// Meta tasks
// ----------
//
// `grunt default`:
// - Execute `grunt dev` task
// - Execute `grunt watch` task
//
// `grunt dev`:
// - Execute `grunt scripts` task
// - Execute `grunt images` task
// - Execute `grunt sass:dev` task
//
// `grunt prod`:
// - Execute `grunt scripts` task
// - Execute `grunt images` task
// - Execute `grunt sass:prod` task
//
//
// Micro tasks
// -----------
//
// `grunt sass:dev`:
// - Convert .sass files to .css files to distrib folder
//
// `grunt sass:prod`:
// - Convert .sass files to .css files and compress them to distrib folder
//
// `grunt scripts`:
// - Concat .js files
// - Minify .js files to distrib folder
//
// `grunt images`:
// - Create sprited .png files 
// - Optimize all image files to distrib folder
//
// `grunt watch`:
// - Watch for .sass and .js files modification, then call `grunt sass:dev` and `grunt scripts` tasks
//
// `grunt jshint`:
// - Check if .js files follow conventions
//
// `grunt clean`:
// - Delete all distrib files

module.exports = function(grunt) {

  // Packages imports
  grunt.loadNpmTasks('grunt-contrib-sass');     // https://github.com/gruntjs/grunt-contrib-sass
  grunt.loadNpmTasks('grunt-contrib-jshint');   // https://github.com/gruntjs/grunt-contrib-jshint, http://www.jshint.com/docs/
  grunt.loadNpmTasks('grunt-contrib-concat');   // https://github.com/gruntjs/grunt-contrib-concat
  grunt.loadNpmTasks('grunt-contrib-uglify');   // https://github.com/gruntjs/grunt-contrib-uglify
  grunt.loadNpmTasks('grunt-contrib-watch');    // https://github.com/gruntjs/grunt-contrib-watch
  grunt.loadNpmTasks('grunt-spritesmith');      // https://github.com/Ensighten/grunt-spritesmith
  grunt.loadNpmTasks('grunt-contrib-imagemin'); // https://github.com/gruntjs/grunt-contrib-imagemin
  grunt.loadNpmTasks('grunt-contrib-clean');    // https://github.com/gruntjs/grunt-contrib-clean

  // Global variables
  var srcPath  = '',
      distPath = '../',
      jsSrcHeader = [
        srcPath + 'js/libs/modernizr.custom.96065.js',
      ],
      jsSrcFooter = [
        srcPath + 'js/scripts.js',
      ];

  // Config
  grunt.initConfig({

    jshint: {
      src: srcPath + 'js/*.js',
      options: {
        browser: true,
        camelcase: true,
        curly: true,
        eqeqeq: true,
        eqnull: true,
        globals: {
          jQuery: true,
          phantom: true
        },
        indent: 2,
        maxlen: 120,
        quotmark: 'single',
        reporter: require('jshint-stylish')
      }
    },

    sass: {
      prod: {
        options: {
          style: 'compressed',
          sourcemap: true // Requires Sass 3.3.0, which can be installed with 'gem install sass --pre'
        },
        files: [{
          expand: true,
          cwd: srcPath + 'scss/',
          src: ["style.scss"],
          dest: distPath,
          ext: '.css'
        }]
      },
      dev: {
        options: {
          style: 'expanded',
          sourcemap: true // Requires Sass 3.3.0, which can be installed with 'gem install sass --pre'
        },
        files: [{
          expand: true,
          cwd: srcPath + 'scss/',
          src: ["style.scss"],
          dest: distPath,
          ext: '.css'
        }]
      }
    },

    concat: {
      options: {
        separator: ';'
      },
      common: {
        files: [
          { src: jsSrcHeader,  dest: distPath + 'js/scripts-header.js' },
          { src: jsSrcFooter,  dest: distPath + 'js/scripts-footer.js' },
        ]
      }
    },

    uglify: {
      options: {
        separator: ';'
      },
      common: {
        files: [
          { src: jsSrcHeader,  dest: distPath + 'js/scripts-header-min.js' },
          { src: jsSrcFooter,  dest: distPath + 'js/scripts-footer-min.js' },
        ]
      }
    },

    watch: {
      js: {
        files: srcPath + 'js/**/*.js',
        tasks: ['scripts']
      },
      css: {
        files: srcPath + 'scss/**/*.scss',
        tasks: ['sass:dev'],
        options: {
          livereload: true
        }
      }
    },

    /* @ToDo
    sprite: {
      icons: {
        src: srcPath + 'images/icons/*.png',
        destImg: srcPath + 'images/sprites-icons.png',
        destCSS: srcPath + 'scss/partials/_core-sprites-icons.scss',
        imgPath: 'images/sprites-icons.png',
        engine: 'pngsmith'
      }
    }, */

    imagemin: {
      all: {
        options: {
          cache: false,
          optimizationLevel: 0
        },
        files: [{
          expand: true,
          cwd: srcPath + 'images/',
          dest: distPath + 'images/',
          src: ['**/*.{png,jpg,gif}']
        }]
      }
    },

    /* @ToDo
    clean: {
      build: {
        src: [distPath + '/*']
      }
    } */

  });

  // Register common tasks
  grunt.registerTask('scripts', ['concat:common', 'uglify:common']);
  grunt.registerTask('images', [/*'sprite:icons',*/ 'imagemin:all']);

  // Register dev task
  grunt.registerTask('dev', ['scripts', 'images', 'sass:dev']);

  // Register prod task
  grunt.registerTask('prod', ['scripts', 'images', 'sass:prod']);

  // Default task
  grunt.registerTask('default', ['dev', 'watch']);

}